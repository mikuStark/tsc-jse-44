package ru.tsc.karbainova.tm.endpoint;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.karbainova.tm.marker.SoapCategory;

import java.util.List;

public class ProjectEndpointTest {
    @NonNull
    private static final SessionEndpointService sessionEndpointService = new SessionEndpointService();
    @NonNull
    private static final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NonNull
    private static final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
    @NonNull
    private static final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @Nullable
    private static SessionDTO session;
    @Nullable ProjectDTO project;
    private static String userLogin = "test";

    @BeforeClass
    public static void beforeClass() {
        session = sessionEndpoint.openSession(userLogin, userLogin);
    }

    @Before
    public void before() {
        project = new ProjectDTO();
        project.setName("Project");
        project.setId("1");
        projectEndpoint.addProject(session, project);
    }

    @After
    public void after() {
        projectEndpoint.removeProjectById(session, "1");
    }

    @AfterClass
    public static void afterClass() {
        sessionEndpoint.closeSession(session);
    }

    @Test
    @Category(SoapCategory.class)
    public void add() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("Project", project.getName());

        @NonNull final ProjectDTO projectById = projectEndpoint.findByNameProject(session, "Project");
        Assert.assertNotNull(projectById);
    }

    @Test
    @Category(SoapCategory.class)
    public void findAll() {
        @NonNull final List<ProjectDTO> projects = projectEndpoint.findAllProject(session);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    @Category(SoapCategory.class)
    public void findAllByUserId() {
        @NonNull final List<ProjectDTO> projects = projectEndpoint.findAllProject(session);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    @Category(SoapCategory.class)
    public void findAllByName() {
        @NonNull final ProjectDTO projects = projectEndpoint.findByNameProject(session, project.getName());
        Assert.assertNotNull(projects);
    }

    @Test
    @Category(SoapCategory.class)
    public void findAllByErrorName() {
        @NonNull final ProjectDTO projects = projectEndpoint.findByNameProject(session, "sdf");
        Assert.assertNull(projects);
    }

    @Test
    @Category(SoapCategory.class)
    public void removeById() {
        projectEndpoint.removeProjectById(session, project.getId());
        Assert.assertNull(projectEndpoint.findByNameProject(session, "Project"));
    }

}
